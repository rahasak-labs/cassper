package com.rahasak.cassper.cassandra

import java.util.Date

import com.dataoperandz.cassper.Cassper

case class Account(id: String, name: String, phone: String, email: String, disabled: Boolean, timestamp: Date = new Date())

object CassandraStore extends CassandraCluster {

  // prepared statements
  lazy val caps = session.prepare("INSERT INTO mystiko.accounts(id, name, phone, email, activated, timestamp) VALUES(?, ?, ?, ?, ?, ?)")
  lazy val gaps = session.prepare("SELECT * FROM mystiko.accounts where id = ? LIMIT 1")

  def init(): Unit = {
    // migrate via cassper
    val builder = new Cassper().build("mystiko", session)
    builder.migrate("mystiko")
  }

  def createAccount(account: Account) = {
    session.execute(caps.bind(account.id, account.name, account.phone, account.email,
      account.disabled: java.lang.Boolean, account.timestamp))
  }

  def getAccount(name: String): Option[Account] = {
    val row = session.execute(gaps.bind(name)).one()
    if (row != null) Option(
      Account(
        row.getString("id"),
        row.getString("name"),
        row.getString("phone"),
        row.getString("email"),
        row.getBool("disabled"),
        row.getTimestamp("timestamp")))
    else None
  }

}
