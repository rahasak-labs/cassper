package com.rahasak.cassper

import com.dataoperandz.cassper.Cassper
import com.rahasak.cassper.cassandra.CassandraCluster

object Main extends App with CassandraCluster {

  // migrate via cassper
  val builder = new Cassper().build("mystiko", session)
  builder.migrate("mystiko")

}

